FROM golang:1.12.4-stretch

RUN go get -t github.com/bradfitz/gomemcache/memcache
RUN go install github.com/bradfitz/gomemcache/memcache
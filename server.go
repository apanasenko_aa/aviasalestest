package main

import (
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"github.com/bradfitz/gomemcache/memcache"
	"log"
	"net/http"
	"os"
	"time"
)

const AVIASALES_API string = "https://places.aviasales.ru/v2/places.json"
const CITY_TYPE string = "city"
const AIRPORT_TYPE string = "airport"

type AviasalesResponseData struct {
	Type string `json:"type"`
	CountryCode string `json:"country_code"`
	CountryName string `json:"country_name"`
	CityCode string `json:"city_code"`
	CityName string `json:"city_name"`
	Code string `json:"code"`
	Name string `json:"name"`
}

type ResponseData struct {
	Slug string `json:"slug"`
	Subtitle string `json:"subtitle"`
	Title string `json:"title"`
}

var (
	client = &http.Client{
		Timeout: 3 * time.Second,
	}

	memcached = memcache.New("host.docker.internal:11211")
	logger = log.New(os.Stdout, "logger: ", log.Lshortfile)
)

func (aviasalesResponseData *AviasalesResponseData) convertToResponse() ResponseData {
	var response ResponseData

	switch aviasalesResponseData.Type {
	case CITY_TYPE:
		response = ResponseData{
			aviasalesResponseData.Code,
			aviasalesResponseData.CountryName,
			aviasalesResponseData.Name,
		}
	case AIRPORT_TYPE:
		response = ResponseData{
			aviasalesResponseData.Code,
			aviasalesResponseData.CityName,
			aviasalesResponseData.Name,
		}
	}

	return response
}

func GenerateCacheKay(data string) string {
	h := sha1.New()
	h.Write([]byte(data))
	return hex.EncodeToString(h.Sum(nil))
}

func PlacesRouterHandler(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	cacheKey := GenerateCacheKay(request.URL.RawQuery)
	resultItem, cacheError := memcached.Get(cacheKey)

	if cacheError != nil {
		logger.Print("Get cache error: ", cacheError)
	}

	if resultItem == nil {
		requestUrl := AVIASALES_API + "?" + request.URL.RawQuery
		rawAviasalesResponse, requestError := client.Get(requestUrl)

		if requestError != nil || rawAviasalesResponse == nil {
			logger.Print("Request Error: ", requestUrl, requestError)
			http.Error(response, "Bad Gateway", 500)
			return
		}

		if rawAviasalesResponse.StatusCode != 200 {
			logger.Print("Request bad status: ", requestUrl, rawAviasalesResponse.Status)
			http.Error(response, rawAviasalesResponse.Status, rawAviasalesResponse.StatusCode)
			return
		}

		defer rawAviasalesResponse.Body.Close()

		var aviasalesResponseData []AviasalesResponseData

		jsonDecadeError := json.NewDecoder(rawAviasalesResponse.Body).Decode(&aviasalesResponseData)

		if jsonDecadeError != nil {
			logger.Print("Decode error: ", jsonDecadeError)
			http.Error(response, "Bad Gateway", 500)
			return
		}

		var responseData []ResponseData
		for _, item := range aviasalesResponseData {
			responseData = append(responseData, item.convertToResponse())
		}

		result, jsonMarshalError := json.Marshal(responseData)
		if jsonMarshalError != nil {
			logger.Print("Marshal error: ", jsonDecadeError)
			http.Error(response, "Bad Gateway", 500)
			return
		}

		resultItem = &memcache.Item{
			Key: cacheKey,
			Value: result,
		}
		cacheError = memcached.Set(resultItem)

		if cacheError != nil {
			logger.Print("Set cache error: ", cacheError)
		}
	}

	_, writeError := response.Write(resultItem.Value)

	if writeError != nil {
		logger.Print("Write error: ", writeError)
	}
}

func main() {
	logger.Println("Start server")
	http.HandleFunc("/places.json", PlacesRouterHandler)
	listenError := http.ListenAndServe(":9000", nil)

	if listenError != nil {
		logger.Print("Listen error: ", listenError)
	}
}
